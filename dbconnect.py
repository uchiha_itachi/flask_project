import pymongo


def connect(col_name):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["mydatabase"]
    mycol = mydb[col_name]
    return mycol


def insert(collection, new_dic):
    collection.insert_one(new_dic)


def get_by_mail(collection, email):
    return collection.find_one({"email": email})


def add_student(collection, student):
    collection.insert_one(student)


def check_student_id(collection, student_id):
    for x in collection.find():
        if student_id == x["_id"]:
            return False
    return True


def sort_by(collection, byvalue):
    if byvalue == "StudentID":
        byvalue = "_id"
    list_student = list(collection.find())
    for i in range(0, len(list_student)-1):
        for j in range(i+1, len(list_student)):
            x = list_student[i][byvalue]
            y = list_student[j][byvalue]
            if (list_student[i][byvalue] > list_student[j][byvalue]):
                t = list_student[i]
                list_student[i] = list_student[j]
                list_student[j] = t
    return list_student


def update_student(studentID, math, physic, english):
    col = connect("student")
    avg = (math+physic+english)/3
    col.update_one({"_id": studentID}, {
                   "$set": {"Math": math, "Physic": physic, "English": english,
                            "Average": avg}
                   })
    # return col.find_one()


def delete_by(collection, studentID):
    collection.delete_one({"_id": studentID})


# coll = connect("student")
# for x in coll.find():
#     print(x)
# coll.drop()
# delete_by(coll, "11")
# print(update_student("1", 10.0, 10.0, 10.0))
# sort_by(coll, "math")
