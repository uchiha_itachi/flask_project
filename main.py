from flask import Flask, render_template, request, session, redirect, \
    url_for, send_file, make_response, flash
from qrcode import make
import qrcode
import pyotp
import io
import werkzeug.security
import dbconnect
import auth
app = Flask(__name__)
app.secret_key = "ajsdnas"


@app.route("/")
def home():
    email = request.cookies.get("email")
    rq = request.headers.get("X-Real-IP")
    mess = rq

    username = None
    if email is not None:
        col = dbconnect.connect("login")
        username = dbconnect.get_by_mail(col, email)["username"]
    return render_template("home.html", email=email, username=username,
                           mess=mess)


@app.route("/register", methods=["POST", "GET"])
def register():
    error = None
    if(request.method == "POST"):
        col = dbconnect.connect("login")
        username = request.form["username"]
        email = request.form["email"]
        password = request.form["password"]
        if (auth.check_register(username, email, password)):
            return render_template('qrcode.html', email=email)
        else:
            error = "Email has been used. Please try another email."
            return render_template("register.html", error=error)
    return render_template("register.html", error=error)


@app.route("/login", methods=["POST", "GET"])
def login():
    error = None
    if (request.method == "POST"):
        email = request.form["email"]
        password = request.form["password"]
        optcode = request.form["optcode"]
        error = auth.check_login(email, password, optcode)
        if error == "OTP is true.":
            resp = make_response(redirect(url_for("home")))
            resp.set_cookie("email", email, max_age=60*60)
            return resp
        if error == "OTP is not true.":
            return render_template("login.html", error=error)
        else:
            return render_template("login.html", error=error)
    return render_template("login.html", error=None)


@app.route("/qrcode/<email>")
def qrcode(email):
    col = dbconnect.connect("login")
    x = dbconnect.get_by_mail(col, email)
    topt = pyotp.TOTP(x["secret_key"])
    qr_code = make(topt.provisioning_uri(email))
    img = io.BytesIO()
    qr_code.save(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')


@app.route("/logout")
def logout():
    email = request.cookies.get("email")
    resp = make_response(redirect(url_for("home")))
    resp.set_cookie("email", email, max_age=0)
    return resp


@app.route("/studentlist", methods=["POST", "GET"])
def studentlist():
    if request.cookies.get("email") is None:
        return redirect(url_for("home"))

    student_col = dbconnect.connect("student")
    if request.method == "POST":
        byvalue = request.form["sortby"]
        student_list = dbconnect.sort_by(student_col, byvalue)
        return render_template("studentlist.html", students=student_list)
    student_list = student_col.find()
    return render_template("studentlist.html", students=student_list)


@app.route("/addstudent", methods=["POST", "GET"])
def addstudent():
    if request.cookies.get("email") is None:
        return redirect(url_for("home"))
    error = None
    if request.method == "POST":
        student_col = dbconnect.connect("student")
        student_id = request.form["studentID"]
        name = request.form["name"]
        date_of_birth = request.form["date"]
        math_score = float(request.form["math"])
        physic_score = float(request.form["physic"])
        english_score = float(request.form["english"])
        avg_score = (math_score + physic_score + english_score) / 3
        check = dbconnect.check_student_id(student_col, student_id)
        if check:
            student_dic = {
                "_id": student_id,
                "Name": name,
                "Date_of_birth": date_of_birth,
                "Math": math_score,
                "Physic": physic_score,
                "English": english_score,
                "Average": avg_score
            }
            dbconnect.add_student(student_col, student_dic)
            return redirect(url_for("studentlist"))
        else:
            error = "StudentID duplicate."
            return render_template("addstudent.html", error=error)

    return render_template("addstudent.html", error=error)


@app.route("/deletestudent/<studentID>", methods=["POST", "GET"])
def deletestudent(studentID):
    if request.cookies.get("email") is None:
        return redirect(url_for("home"))
    if request.method == "POST":
        col = dbconnect.connect("student")
        dbconnect.delete_by(col, str(studentID))
    return redirect(url_for("studentlist"))


@app.route("/update/<studentID>", methods=["POST", "GET"])
def update(studentID):
    if request.cookies.get("email") is None:
        return redirect(url_for("home"))
    if request.method == "POST":
        math = float(request.form["math"])
        physic = float(request.form["physic"])
        english = float(request.form["english"])
        dbconnect.update_student(studentID, math, physic, english)
    return redirect(url_for("studentlist"))


if __name__ == '__main__':
    app.run(debug=True)
