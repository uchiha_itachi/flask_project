import dbconnect
import pyotp
import werkzeug.security


def check_register(username, email, password):
    col = dbconnect.connect("login")
    secret_key = pyotp.random_base32()
    user = dbconnect.get_by_mail(col, email)
    if (user is None):
        hasspass = werkzeug.security.generate_password_hash(password)
        dic = {
            "username": username,
            "email": email,
            "password": hasspass,
            "secret_key": secret_key
        }
        dbconnect.insert(col, dic)
        return True
    return False


def check_login(email, password, otpcode):
    col = dbconnect.connect("login")
    x = dbconnect.get_by_mail(col, email)

    if x is not None:
        hasspass = werkzeug.security.check_password_hash(
            x["password"], password)
        if hasspass:
            totp = pyotp.TOTP(x["secret_key"])
            if (totp.verify(str(otpcode))):
                return "OTP is true."
            else:
                return "OTP is not true."
    return "Email or password is invalid."
